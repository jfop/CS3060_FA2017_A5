fruit(pineapple).
fruit(apple).
fruit(banana).
fruit(carrot).
fruit(cherry).

junk(pizza).
junk(icecream).
junk(burger).
junk(fries).
junk(cake).

drink(soda).
drink(water).
drink(milk).
drink(orangejuice).
drink(beer).

unhealthy(X):-junk(X).
unhealthy(soda):-drink(soda).
unhealthy(beer):-drink(beer).

healthy(X):-fruit(X).
healthy(water):-drink(water).
healthy(milk):-drink(milk).
healthy(orangejuice):-drink(orangejuice).

liquid(X):-drink(X).
liquid(soda):-unhealthy(soda).
liquid(beer):-unhealthy(beer).
liquid(water):-healthy(water).
liquid(milk):-healthy(milk).
liquid(orangejuice):-healthy(orangejuice).
